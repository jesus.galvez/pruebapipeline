@IngresoSodimac
Feature: historia 001

  @ingresoHome
  Scenario Outline: ingresar a sodimac
    Given ingreso a sodimac <url>
    Then valido estar en sitio de sodimac

    Examples: 
      | url                                  |
      | "https://www.sodimac.cl/sodimac-cl/" |

  Scenario Outline: selecciono menu
    Given verifico que estoy en home de sodimac<url>
    When Selecciono menu<menu>
    Then valido seleccion de menu<menu>

    @casos @validacionFormulariCl 
    Examples: 
      | menu                        | url                                  |
      | "Construcción y Ferretería" | "https://www.sodimac.cl/sodimac-cl/" |
    @pinturas @regresionMenus
    Examples: 
      | menu                              | url                                  |
      | "Pisos, Pinturas y Terminaciones" | "https://www.sodimac.cl/sodimac-cl/" |

    @herramietas @regresionMenus
    Examples: 
      | menu                         | url                                  |
      | "Herramientas y Maquinarias" | "https://www.sodimac.cl/sodimac-cl/" |

    @baño @regresionMenus
    Examples: 
      | menu                      | url                                  |
      | "Baño, Cocina y Limpieza" | "https://www.sodimac.cl/sodimac-cl/" |
