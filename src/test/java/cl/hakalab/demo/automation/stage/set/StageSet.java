package cl.hakalab.demo.automation.stage.set;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features/",//ubicacion de los features
        glue = "cl/hakalab/demo/automation/definition/"//ubicacion de las definiciones de los features
        
)
public class StageSet {

}
