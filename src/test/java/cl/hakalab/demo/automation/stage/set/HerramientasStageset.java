package cl.hakalab.demo.automation.stage.set;

import java.io.File;
import java.io.IOException;

import org.junit.AfterClass;
import org.junit.runner.RunWith;


import com.cucumber.listener.Reporter;


import cl.hakalab.demo.automation.dataProvider.ConfigFileReader;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;



@CucumberOptions(tags = {"@herramietas"}//tag de ejecucion de nuestro feature
, plugin = { "pretty:target/reportes/pretty/pretty.txt",
		"html:target/reportes/html", "json:target/reportes/json/report.json", "junit:target/reportes/junit/junit.xml",
		"usage:target/reportes/usage/usage.json", "rerun:target/reportes/rerun/rerun.txt",
		"com.cucumber.listener.ExtentCucumberFormatter:Reporte/ReporteDemo.html" })//generacion de reporte , ruta donde sera almacenado
//152.139.28.77/ResultadosSeleniumTest/
public class HerramientasStageset extends StageSet {
	
	
	@AfterClass
	public static void writeExtentReport() throws IOException {
		Reporter.loadXMLConfig(new File(ConfigFileReader.getInstance().getReportConfigPath()));//creacion de reporte personalizado

	}
}
