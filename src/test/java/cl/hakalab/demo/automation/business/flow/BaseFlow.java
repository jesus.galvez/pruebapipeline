package cl.hakalab.demo.automation.business.flow;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;


import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.support.PageFactory;


import cl.hakalab.demo.automation.model.PageObjectModelDemo;





@RunWith(Suite.class)
public class BaseFlow {//nombre de nuestra clase
	
	
	public static RemoteWebDriver driver;//declaracion de variable que contendra driver de ejecucion 
    public static PageObjectModelDemo demo;//ejemplo para instanciar variable con clase modelo creado en package model
	static DesiredCapabilities caps = null;//variable que contendra la configuracion interna de nuestro driver
	public static final String USERNAME = "hugoHaka";//nombre de usuario Saucelab granja de dispositivos
	public static final String ACCESS_KEY = "cf258cd2-347e-4954-afea-0f3864b4ec4b";//clave de acceceso mediante script a saucelab
	public static final String URLs = "https://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:443/wd/hub";//url de ejecucion saucelab
	
	
	@BeforeClass//tag que indica que este metodo se ejecutara antes que cualquier otro al iniciar la ejecucion 
	public static void InitializeWebDriver() throws Exception {//nombre de metodo donde se iniciara nuestro driver para comenzar la ejecucion
		
		//if (System.getenv("hub").equals("local")) {//if que decide el hub de ejecucion por variable de entorno utilizada en integracion con jenkins
		//setDriver();
		//}else {//se ejecuta este trozo de codigo en caso de que no se ejecute en el equipo local sino en un hub dentro de un servidor
		//	capabilityBrowser(System.getenv("navegador"));//este metodo define propiedades de ejecucion en el driver para saucelab mediante una variable de entorno
		//	capabilityBrowser("iExplore");//este metodo define propiedades de ejecucion en el driver para saucelab mediante un string en duro
//			driver = new RemoteWebDriver(new java.net.URL(URLs), caps);//se le carga a la variable driver la configuracion de navegador y url de hub donde se ejecutara
	//	}
			setDriver();
		//ManagementMicrosoftService.createNewFileExcel();//instancia de metodo para creacion de driver
		demo=PageFactory.initElements(driver,PageObjectModelDemo.class);//se instancia clase creada con los objetos de prueba en el package model asi cargar la variable creada mas arriba
		
	}
	    
//}
	

	@AfterClass//tag que indica que este metodo se ejcutara al final de la ejecucion
	public static void setUpFinal() throws Exception {
	driver.quit();//metodo que cierra el driver y el navegador
						
	}

	public static void setDriver() throws MalformedURLException {//metodo que permite ejecutar en un hub local o remoto
       	 DesiredCapabilities capabilities = DesiredCapabilities.chrome();//se le indica la opcion de navegador que tendra el driver
      // driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),capabilities);//se le indica al driver que se ejecutara en un hub de selenium local con ciertas condiciones del driver
    	 driver = new RemoteWebDriver(new URL("http://cd04e6fe.ngrok.io/wd/hub"), capabilities);//se le indica al driver que se ejecutara en un hub de selenium en algun servidor remoto con cierta ip con ciertas condiciones del driver
         driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
         driver.manage().timeouts().pageLoadTimeout(500, TimeUnit.SECONDS);
    }

	public static void setDriverDesa() throws Exception {//metodo que define la ejecucion de un driver local
		switch ("chrome") {//aqui se le indica que driver sera el ejecutado de manera local puede pasarse por variable de entorno
		
		case "chrome":
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"/Webdriver/WebDriver/Chrome/Windows/chromedriver.exe" );
			driver = new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			
			break;
		
		case "iExplorer":
			DesiredCapabilities caps = DesiredCapabilities.internetExplorer();    
			caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);    
			caps.setCapability("requireWindowFocus", true);
			driver = new InternetExplorerDriver(caps);
			driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			Runtime.getRuntime().exec(System.getProperty("user.dir")+"\\WebDriver\\WebDriver\\IExplorer\\Windows\\aut.exe");
			
			break;
		
		}
		
	}
	
	
	public static Capabilities capabilityBrowser(String dispositivo) {//metodo que decide configuracion de driver para saucelab
		switch (dispositivo) {
		case "firefox":
			caps = DesiredCapabilities.firefox();
			caps.setCapability("platform", "Windows 10");
			caps.setCapability("version", "61.0");
			break;
		case "chrome":
			caps = DesiredCapabilities.chrome();
			caps.setCapability("platform", "Windows 10");
			caps.setCapability("version", "67.0");
			break;
		case "iExplore":
			caps = DesiredCapabilities.internetExplorer();
			caps.setCapability("platform", "Windows 10");
			caps.setCapability("version", "11.103");
		break;
		case "android 7.1":
			caps = DesiredCapabilities.android();
			caps.setCapability("appiumVersion", "1.9.1");
			caps.setCapability("deviceName","Samsung Galaxy S9 Plus WQHD GoogleAPI Emulator");
			caps.setCapability("deviceOrientation", "portrait");
			caps.setCapability("browserName", "Chrome");
			caps.setCapability("platformVersion", "7.1");
			caps.setCapability("platformName","Android");
		case "iphone":
			 caps = DesiredCapabilities.iphone();
			caps.setCapability("appiumVersion", "1.9.1");
			caps.setCapability("deviceName","iPhone XS Simulator");
			caps.setCapability("deviceOrientation", "portrait");
			caps.setCapability("platformVersion","12.0");
			caps.setCapability("platformName", "iOS");
			caps.setCapability("browserName", "Safari");
		break;
		}

		return caps;

	}
	
	
}
