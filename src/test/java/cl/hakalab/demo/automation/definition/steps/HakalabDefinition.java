package cl.hakalab.demo.automation.definition.steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cl.hakalab.demo.automation.business.flow.BaseFlow;
import cl.hakalab.demo.automation.conn.json.CommJson;
import cl.hakalab.demo.automation.util.GenericMethods;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class HakalabDefinition {

	private static final Logger LOGGER = LoggerFactory.getLogger("");//creacion de variable logger log para escribir 

	@cucumber.api.java.Before//tag que indica que este metodo se ejecutara antes de cada scenario
	public void escribirLog(Scenario scenario) {//metodo que escribira el nombre del scenario
		LOGGER.info("Inicio de escenario : " + scenario.getName());
	}

	@After//tag que indica que se ejcutara antes de cada metodo
	public void loadImage(Scenario scenario) throws Exception {//metodo que escribira log al final de cada scenario
		LOGGER.info("Estado del escenario: " + scenario.getStatus());//log que escribe status del scenario
		GenericMethods.screenShotForScenarioFailed(scenario);//metodo que captura screenshot en caso de pasos fallidos y los inyecta en el reporte
	}

	

}
