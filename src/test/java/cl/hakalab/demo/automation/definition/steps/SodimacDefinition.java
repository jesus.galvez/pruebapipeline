package cl.hakalab.demo.automation.definition.steps;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import cl.hakalab.demo.automation.business.flow.BaseFlow;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SodimacDefinition {

	public  boolean men=false;
	@Given("^ingreso a sodimac \"(.*?)\"$")
	public void ingreso_a_sodimac(String url) throws Throwable {
		
	  BaseFlow.driver.get(url);
	}

	@Then("^valido estar en sitio de sodimac$")
	public void valido_estar_en_sitio_de_sodimac() throws Throwable {
	 assertTrue(BaseFlow.driver.findElement(By.id("NewLogo")).isDisplayed());
	}

	@Given("^verifico que estoy en home de sodimac\"(.*?)\"$")
	public void verifico_que_estoy_en_home_de_sodimac(String url) throws Throwable {
		Thread.sleep(2000);
	    if (!BaseFlow.driver.getCurrentUrl().equals(url)) {
	    	  BaseFlow.driver.get(url); 
	    	 
	    	  
		}
	}

@When("^Selecciono menu\"(.*?)\"$")
	public void selecciono_menu(String menuSel) throws Throwable {
	Thread.sleep(2000);
	   List<WebElement> menus=BaseFlow.driver.findElement(By.className("navBar-content")).findElements(By.tagName("li"));
	   for (WebElement menu:menus) {
		if (menu.getText().equals(menuSel.toUpperCase())) {
			
			 Actions actions = new Actions(BaseFlow.driver);
		     actions.moveToElement(menu).perform();
			Thread.sleep(2000);
			
			if(menu.getAttribute("class").contains("optionActive")) {
				men=true;
				break;
			}
			
		}
	}
	}

	@Then("^valido seleccion de menu\"(.*?)\"$")
	public void valido_seleccion_de_menu(String arg1) throws Throwable {
assertTrue(men);
	}
}
