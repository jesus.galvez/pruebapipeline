package cl.hakalab.demo.automation.util;

import java.util.List;

public class PersonaSingletonBd {

	public String rut;
	public String telefono;
	public String tipoDespacho;
	public String fechaCompra;
	public String email;
	public String modelo;
	public String precio;
	public String region;
	public String ciudad;
	public String comuna;
	public String numero;
	public String Direccion;
	
	public String getDireccion() {
		return Direccion;
	}
	public void setDireccion(String direccion) {
		Direccion = direccion;
	}
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getTipoDespacho() {
		return tipoDespacho;
	}
	public void setTipoDespacho(String tipoDespacho) {
		this.tipoDespacho = tipoDespacho;
	}
	public String getFechaCompra() {
		return fechaCompra;
	}
	public void setFechaCompra(String fechaCompra) {
		this.fechaCompra = fechaCompra;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getPrecio() {
		return precio;
	}
	public void setPrecio(String precio) {
		this.precio = precio;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getComuna() {
		return comuna;
	}
	public void setComuna(String comuna) {
		this.comuna = comuna;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	




//	private static PersonaSingletonBd instance = null;
//
//	public static void setInstance(PersonaSingleton instance) {
//		PersonaSingleton.instance = instance;
//	}
//
//	public static PersonaSingletonBd getInstance() {
//		if (instance == null) {
//			instance = new PersonaSingletonBd();
//		}
//		return instance;
//	}

}
