package cl.hakalab.demo.automation.util;

public class PersonaSingleton {


	public String Rut;
	public String tipoDeCliente;
	public String nombres;
	public String apellidoMaterno;
	public String apellidoPaterno;
	public String estadoCivil;
	public String telefonoFijo;
	public String telefonoMovil;
	public String extranjero;
	public String correo;
	public String tipoDireccion;
	public String direccion;
	public String comuna;
	public String region;
	public String ciudad;
	public String numero;
	public String complentoDireccion;

	static PersonaSingleton instance = null;
	public String getTipoDeCliente() {
		return tipoDeCliente;
	}	
	

	public static void setInstance(PersonaSingleton instance) {
		PersonaSingleton.instance = instance;
	}

	public static PersonaSingleton getInstance() {
		if (instance == null) {
			instance = new PersonaSingleton();
		}
		return instance;
	}

}
