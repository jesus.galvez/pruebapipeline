package cl.hakalab.demo.automation.util;

import org.json.JSONArray;
import org.json.JSONObject;

import cl.hakalab.demo.automation.conn.json.CommJson;




public class Sodimac {
	
	public static void main(String args[]) throws Exception {
		PruebaConceptoApiBody("https://api-sodimac-pgs.buffetcloud.io/pgs-dte-testing/ws/product/get");
	}
	

	public static int PruebaConceptoApi(String endpoint) throws Exception {
		System.out.println(endpoint);		
		CommJson service = new CommJson();
		service.setConn(endpoint);
		service.setRequestMethod("POST");
		service.setRequestProperty("Content-Type", "application/json");
		service.setRequestProperty("x-api-key", "uYHSf748XexLAmQl2QnjxmX9d9g");
		service.setRequestProperty("x-tenant-id", "CL");
		service.setRequestProperty("User-Agent",
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36");
	
		String json = "{\"country\":\"CL\",\"storeId\":\"067\",\"sku\":\"\",\"name\":\"\"}\"";
		service.postParametros(json);
		int status = service.getResponseCode();
		
		if (status == 200 || status == 201) {
			return status;
		} else {
			return status;
		}
	}
	
	
	
	public static JSONArray PruebaConceptoApiBody(String endpoint) throws Exception {
		CommJson service = new CommJson();
		
		JSONObject retorno = null;
		
		
		service.setConn(endpoint);
		service.setRequestMethod("POST");
		service.setRequestProperty("Content-Type", "application/json");
		service.setRequestProperty("x-api-key", "uYHSf748XexLAmQl2QnjxmX9d9g");
		service.setRequestProperty("x-tenant-id", "CL");
		service.setRequestProperty("User-Agent",
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36");
	
		String json = "{\"country\":\"CL\",\"storeId\":\"067\",\"sku\":\"\",\"name\":\"\"}\"";
		service.postParametros(json);
		int status = service.getResponseCode();
		
		if (status == 200 || status == 201) {
			String jso = service.getJsonData();
			JSONObject JsonObj = new JSONObject(jso);
			JSONArray body = JsonObj.getJSONArray("items");
			for (int i = 0; i < body.length(); i++) {
				JSONObject ResultObj=body.getJSONObject(i);
				System.out.println("item id : "+ResultObj.get("ItemId"));
				System.out.println("Name : "+ResultObj.get("Name"));
				System.out.println("StockCount : "+ResultObj.get("StockCount"));

			}
			return body;
		} else {
			JSONArray body = null;
			return body;
		}
	}

}
