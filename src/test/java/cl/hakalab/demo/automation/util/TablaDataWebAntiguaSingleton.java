package cl.hakalab.demo.automation.util;

import java.util.ArrayList;


public class TablaDataWebAntiguaSingleton {


	public  ArrayList<String> monedFondo ;
	public  ArrayList<String> valorOrigen;
	public  ArrayList<String> tipoCambio;
	public  ArrayList<String> valorPesos;
	public  ArrayList<String> rentabilidad;

	static TablaDataWebAntiguaSingleton instance = null;
	

	
	

	



	public ArrayList<String> getMonedFondo() {
		return monedFondo;
	}







	public void setMonedFondo(ArrayList<String> monedFondo) {
		this.monedFondo = monedFondo;
	}







	public ArrayList<String> getValorOrigen() {
		return valorOrigen;
	}







	public void setValorOrigen(ArrayList<String> valorOrigen) {
		this.valorOrigen = valorOrigen;
	}







	public ArrayList<String> getTipoCambio() {
		return tipoCambio;
	}







	public void setTipoCambio(ArrayList<String> tipoCambio) {
		this.tipoCambio = tipoCambio;
	}







	public ArrayList<String> getValorPesos() {
		return valorPesos;
	}







	public void setValorPesos(ArrayList<String> valorPesos) {
		this.valorPesos = valorPesos;
	}







	public ArrayList<String> getRentabilidad() {
		return rentabilidad;
	}







	public void setRentabilidad(ArrayList<String> rentabilidad) {
		this.rentabilidad = rentabilidad;
	}







	public static TablaDataWebAntiguaSingleton getInstance() {
		if (instance == null) {
			instance = new TablaDataWebAntiguaSingleton();
		}
		return instance;
	}

}
