package cl.hakalab.demo.automation.model;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class PageObjectModelDemo {

	
	@FindBy(how = How.ID, using = "continuar_paso_2")//identificador del objeto web puede ser xpath , id, name , etc...
	private WebElement botonCredito;// nombre del objeto creado

	public WebElement getBotonCredito() {//get del objeto 
		return botonCredito;
	}

	public void setBotonCredito(WebElement botonCredito) {//set del objeto
		this.botonCredito = botonCredito;
	}

	
	
	
}