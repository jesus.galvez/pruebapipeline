package cl.hakalab.demo.automation.conn.db;

import java.sql.Connection;
import java.sql.DriverManager;

public class DbConnFactoryServer {
	// Librería de MySQL
    public String driver = "com.mysql.cj.jdbc.Driver";
    // Nombre de la base de datos
    public static String database = "Tefam";
    // Host
    public static String hostname = "10.24.104.172\\SS05";
    // Ruta de nuestra base de datos (desactivamos el uso de SSL con "?useSSL=false")
    public static String url = "jdbc:sqlserver://" + hostname + ";databaseName=" + database ;
    // Nombre de usuario
    public static String username = "sql_config";
    // Clave de usuario
    public static String password = "Ayuda12345";
    
    public static Connection getConectar(){
        Connection cn=null;
        try {
        	//String url = "jdbc:sqlserver://MYPC\\SQLEXPRESS;databaseName=MYDB";
        	Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        	Connection conn = DriverManager.getConnection(url, username, password);
        } 
        catch (Exception e) {
            //e.printStackTrace();
            System.out.println("ERROR"+e.getMessage());
        }
        return cn;
    }
    
    public static void main (String[]args) {
    	DbConnFactoryServer.getConectar();
    }
}